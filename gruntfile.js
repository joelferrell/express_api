module.exports = function(grunt) {

  // Load Grunt tasks declared in the package.json file
  //require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);


  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    sass: {
      dist: {
        files: {
          'public/stylesheets/application.css': 'sass/application.scss'
        },
        loadPath: 'bower_components/fontawesome/scss',
        options: {
          sourcemap: 'auto',
          style: 'expanded',
          lineNumbers: false,
          noCache: true
        }
      }
    },
    watch: {
      source: {
        files: ['sass/**/*.scss', 'public/stylesheets/**/*.css', 'views/**/*.ejs', 'public/javascripts/**/*.{js,json}', 'public/images/**/*.{png,jpg,jpeg,gif,webp,svg}' ],
        tasks: ['sass'],
        options: {
          livereload: true
        }
      }
    },
    express: {
      all: {
        options: {
          port: 3000,
          hostname: "localhost",
          bases: [__dirname] // Replace with the directory you want the files served from
                             // Make sure you don't use `.` or `..` in the path as Express
                             // is likely to return 403 Forbidden responses if you do
                             // http://stackoverflow.com/questions/14594121/express-res-sendfile-throwing-forbidden-error
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['sass']);
};
