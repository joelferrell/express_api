var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.route('/:echo').get(function(req, res) {
  res.json({ echo: req.params.echo });
});

router.post('/', function(req, res, next) {
  res.json({message: "It workied, show me the JSON" });
})

module.exports = router;
