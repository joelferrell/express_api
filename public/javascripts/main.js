(function($){
  "use strict";

  var $html = $('html, body'),
      $winWidth = $(window).width(),
      Modernizr = window.Modernizr,
      transEndEventNames = {
        'WebkitTransition': 'webkitTransitionEnd',
        'MozTransition': 'transitionend',
        'msTransition': 'MSTransitionEnd',
        'transition': 'transitionend'
      },
      transEndEventName = transEndEventNames[Modernizr.prefixed('transition')],
      animEndEventNames = {
        'WebkitAnimation': 'webkitAnimationEnd',
        'MozAnimation': 'MozAnimationEnd',
        'msAnimation': 'MSAnimationEnd',
        'animation': 'animationend'
      },
      animEndEventName = animEndEventNames[Modernizr.prefixed('animation')];

  $.extend({
    getUrlVars: function(){
      var vars = [], hash;
      var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
      for(var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
      }
      return vars;
    },
    getUrlVar: function(name){
    return $.getUrlVars()[name];
    }
  });


  function is_touch_device() {
    return Modernizr.touch;
  }



  if ( navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) ||
    navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) ||
    navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) ||
    navigator.userAgent.match(/Windows Phone/i) ){
    var touch_device = true;
  } else {
    var touch_device = false;
  }





  $(document).ready(function() {



  });


})(jQuery);





